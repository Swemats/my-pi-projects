import serial

port = "/dev/ttyACM0"
serialFromArduino = serial.Serial(port,9600)
serialFromArduino.flushInput()

numberOfSamples = 0
notEnoughSamples = True

while notEnoughSamples:
	if (serialFromArduino.inWaiting() > 0):
		input = serialFromArduino.readline()
		inputAsInteger = int(input)
		print(inputAsInteger * 1)
		numberOfSamples = numberOfSamples + 1
		if (numberOfSamples == 20):
			notEnoughSamples = False
		